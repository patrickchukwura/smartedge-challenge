// Coded and submitted by Patrick Chukwura
package main

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

// SmartEdge Code challenge app
// This app creates a signature based on user input up to 250 characters and returns
// JSON response.
//
// The JSON response includes the original user input, the cryptographic signature of that input
// and the generated public key.
//
// This app will generate an RSA public/private pair if one is not found in the file system.
//

// PublicKeyFileName filename of public key file
const PublicKeyFileName = "public.key"

// PrivateKeyFileName filename of private key file
const PrivateKeyFileName = "private.key"

const keyBitSize = 4096

// KeyPair is a convenience struct to contain RSA public/private pair
type KeyPair struct {
	PublicKey  *rsa.PublicKey
	PrivateKey *rsa.PrivateKey
}

func main() {
	args := os.Args

	fmt.Println("Arguments passed: ", args)

	if len(args) <= 1 {
		log.Fatalln("Please run with argument. i.e. `./app test@me.com`")
	}

	input := args[1]

	// input only up to 250 characters accepted
	if len(input) > 250 {
		log.Fatalln("Your input is too large, make sure it's under 250 characters")
	}

	rsaKeyPair := FetchKeys()
	sig := GenerateSignature(input, rsaKeyPair)
	pubKeyStr := FetchPublicKeyStr(rsaKeyPair)

	payload := map[string]string{"message": input, "signature": sig, "pubkey": pubKeyStr}
	json, err := json.Marshal(payload)

	handleError(err)
	fmt.Println(string(json))
}

// GenerateSignature returns cryptographic signature of `message` using PrivateKey of provided `rsaKeyPair`
func GenerateSignature(message string, rsaKeyPair KeyPair) string {
	hash := sha256.Sum256([]byte(message))
	signature, err := rsa.SignPKCS1v15(rand.Reader, rsaKeyPair.PrivateKey, crypto.SHA256, hash[:])
	handleError(err)

	// let's verify just to be sure (probably not necessary in this code challenge)
	err = rsa.VerifyPKCS1v15(rsaKeyPair.PublicKey, crypto.SHA256, hash[:], signature)
	handleError(err)

	return base64.StdEncoding.EncodeToString(signature)
}

// FetchPublicKeyStr returns public key in string (PEM) format from given `rsaKeyPair`
func FetchPublicKeyStr(rsaKeyPair KeyPair) string {
	pubData, err := ioutil.ReadFile(PublicKeyFileName)
	handleError(err)

	publicPemBlock, _ := pem.Decode(pubData)
	pubStr := base64.StdEncoding.EncodeToString(publicPemBlock.Bytes)
	output := fmt.Sprintf("-----BEGIN %s-----\n%s\n-----END %s-----\n: ",
		publicPemBlock.Type, pubStr, publicPemBlock.Type)

	return output
}

// FetchKeys fetches the current keys. If keys do not exist generates new key pair and fetches
func FetchKeys() KeyPair {
	privData, privErr := ioutil.ReadFile(PrivateKeyFileName)
	if privErr == nil {
		privatePemBlock, _ := pem.Decode(privData)
		privKey, err := x509.ParsePKCS1PrivateKey(privatePemBlock.Bytes)
		handleError(err)

		rsaKeyPair := KeyPair{PublicKey: &privKey.PublicKey, PrivateKey: privKey}
		return rsaKeyPair

	}

	reader := rand.Reader
	key, err := rsa.GenerateKey(reader, keyBitSize)
	handleError(err)

	rsaKeyPair := KeyPair{PrivateKey: key, PublicKey: &key.PublicKey}
	saveKeys(rsaKeyPair)

	return rsaKeyPair
}

// saveKeys saves encompassing public and private key pair in `rsaKeyPair` struct to file
func saveKeys(rsaKeyPair KeyPair) {

	// Save public key
	publicKeyBytes := x509.MarshalPKCS1PublicKey(rsaKeyPair.PublicKey)
	publicPemBlock := pem.Block{Type: "RSA PUBLIC KEY", Bytes: publicKeyBytes}

	pubFile, err := os.Create(PublicKeyFileName)
	defer pubFile.Close()

	handleError(err)
	pem.Encode(pubFile, &publicPemBlock)

	// Save private key
	privateKeyBytes := x509.MarshalPKCS1PrivateKey(rsaKeyPair.PrivateKey)
	privatePemBlock := pem.Block{Type: "RSA PRIVATE KEY", Bytes: privateKeyBytes}

	privFile, err := os.Create(PrivateKeyFileName)
	defer privFile.Close()

	handleError(err)
	pem.Encode(privFile, &privatePemBlock)
}

// handleError checks if error exists in `err`, if so fatals and prints error message
func handleError(err error) {
	if err != nil {
		log.Fatalln("Fatal error: ", err.Error())
	}
}
