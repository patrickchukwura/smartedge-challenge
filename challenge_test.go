// Coded and submitted by Patrick Chukwura
package main

// Tests the code challenge app.
// To utilize test with continuous integration system run `go test -cover`
// to run through tests and get coverage report
// To run tests with a CI system such as TravisCi see  https://docs.travis-ci.com/user/languages/go/

import (
	"os"
	"testing"
)

// Ensure we cleanup after each test
func cleanup() {
	os.Remove(PublicKeyFileName)
	os.Remove(PrivateKeyFileName)
}

//TestGenerateSignature ensures the signature generates
func TestGenerateSignature(t *testing.T) {
	rsaKeyPair := FetchKeys()
	signature := GenerateSignature("message", rsaKeyPair)

	if len(signature) == 0 {
		t.Error("Should have returned signature")
	}
	cleanup()
}

// TestFetchPublicKeyStr ensures the public key is generated and string format of it returns
func TestFetchPublicKeyStr(t *testing.T) {
	rsaKeyPair := FetchKeys()
	pubKeyStr := FetchPublicKeyStr(rsaKeyPair)

	if len(pubKeyStr) == 0 {
		t.Error("Should have returned public key string")
	}
	cleanup()
}

// TestFetchKeysWhenExists ensures that KeyPair is generated and returned even if public/private key already created
func TestFetchKeysWhenExists(t *testing.T) {
	FetchKeys()
	rsaKeyPair := FetchKeys()

	if rsaKeyPair.PublicKey == nil || rsaKeyPair.PrivateKey == nil {
		t.Error("KeyPair content should exist if keys have been previously generated")
	}
	cleanup()
}

// TestFetchKeys ensures KeyPair is returned when fetching
func TestFetchKeys(t *testing.T) {
	rsaKeyPair := FetchKeys()

	if rsaKeyPair.PublicKey == nil || rsaKeyPair.PrivateKey == nil {
		t.Error("KeyPair content should exist")
	}
	cleanup()
}
